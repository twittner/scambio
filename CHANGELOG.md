# 0.2.1

- Add predicates to `Error`.

# 0.2.0

- Removed `Unpin` bound for values which are exchanged.
- Removed `Token` type.
- Renamed `try_send` to `send_now`.

# 0.1.0

Initial release.
