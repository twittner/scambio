# Scambio

Safe and efficient value exchanges between endpoint pairs.

Documentation is available at

- <https://twittner.gitlab.io/scambio/scambio/>
- <https://docs.rs/scambio/>

# License

This project is licensed under the [Blue Oak Model License Version 1.0.0][1].
If you are interested in contributing to this project, please read the file
CONTRIBUTING.md first.

[1]: https://blueoakcouncil.org/license/1.0.0

