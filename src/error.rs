use std::{error, fmt};

/// Possible errors when exchanging data.
#[derive(Clone)]
pub enum Error<T> {
    /// The other end of the exchange has been dropped.
    Closed(T),
    /// The exchange has no more capacity to hold a data item.
    AtCapacity(T)
}

impl<T> Error<T> {
    /// Is this an `Error::Closed`?
    pub fn is_closed(&self) -> bool {
        matches!(self, Error::Closed(_))
    }

    /// Is this an `Error::AtCapacity`?
    pub fn is_full(&self) -> bool {
        matches!(self, Error::AtCapacity(_))
    }

    /// Consume this error and return the data item.
    pub fn into_data(self) -> T {
        match self {
            Error::Closed(val) => val,
            Error::AtCapacity(val) => val
        }
    }
}

impl<T> fmt::Debug for Error<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Closed(_) => f.write_str("Error::Closed"),
            Error::AtCapacity(_) => f.write_str("Error::AtCapacity")
        }
    }
}

impl<T> fmt::Display for Error<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl<T> error::Error for Error<T> {}
